const Discord = require('discord.js');
const QuickChart = require('quickchart-js');
const client = new Discord.Client();
const config = require('./config.json');
//const fetch = require('node-fetch');


let prefix = '!';
function getRandomItem(arr) {

    // get random index value
    const randomIndex = Math.floor(Math.random() * arr.length);

    // get random item
    const item = arr[randomIndex];

    return item;
}

client.on('ready', async () => {
	client.user.setPresence({
		status: 'online',
		activity: {
			name: `Redpill`,
			type: 'PLAYING',
		}
	});
})

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
    if (msg.content === '!redpill')
	{
            fetch('https://redpillapi.com/api/v1/redpill')
             .then(response => response.json())
             .then(data => ReplyRedpill(data));
        function ReplyRedpill(data)
        {
	    msg.reply(data.data);
        }
	}
})

client.on('message', msg => {
    if (msg.content === '!blackpill')
	{
            fetch('https://redpillapi.com/api/v1/redpill/?blackpill=true')
             .then(response => response.json())
             .then(data => ReplyRedpill(data));
        function ReplyRedpill(data)
        {
	    msg.reply(data.data);
        }
	}
})

client.on('message', msg => {
    if (msg.content === '!basedbible')
	{
            fetch('https://redpillapi.com/api/v1/basedbible/?lang=en&random=true')
             .then(response => response.json())
             .then(data => ReplyRedpill(data));
        function ReplyRedpill(data)
        {
	    msg.reply(data.data);
        }
	}
})

 // GET ROLE COUNT
 client.on('message', msg => {
     if(msg.content == "!graph")
     {
         let guild = msg.guild.members.fetch();
         let filosofID = "957274841185157160";
         let natSocID = "949060310323585024";
         let ancapID = "957271462744559631";
         let liberalID = '949060312936644680';
         let konservativID = "949060311355392071";
         let SocialistID = "957270764879499294";
         let kommunistID = "957270914871988304";
         //let globalistID = "957271073198583878";
         //let AnakistID = "957271234339561562";
         let FilosofCount = msg.guild.roles.cache.get(filosofID).members.size;
         let natSocCount = msg.guild.roles.cache.get(natSocID).members.size;
         let ancapCount = msg.guild.roles.cache.get(ancapID).members.size;
         let liberalCount = msg.guild.roles.cache.get(liberalID).members.size;
         let konservativCount = msg.guild.roles.cache.get(konservativID).members.size;
         let SocialistCount = msg.guild.roles.cache.get(SocialistID).members.size;
         let KommunistCount = msg.guild.roles.cache.get(kommunistID).members.size;

//	 let TotalMemberWithRole = FilosofCount + natSocCount + ancapCount + liberalCount + konservativCount + SocialistCount + kommunistCount;

	 let serverCount = msg.guild.memberCount;

	     let FilosofPercentage = (FilosofCount / serverCount * 100).toFixed(2);
	     let NatSocPercentage = (natSocCount / serverCount * 100).toFixed(2);
	     let AncapPercentage = (ancapCount / serverCount * 100).toFixed(2);
	     let LiberalPercentage = (liberalCount / serverCount * 100).toFixed(2);
	     let KonservativPercentage = (konservativCount / serverCount * 100).toFixed(2);
	     let SocialistPercentage = (SocialistCount / serverCount * 100).toFixed(2);
	     let KommunistPercentage = (KommunistCount / serverCount * 100).toFixed(2);

         let MembersWithIdeologi = FilosofCount + natSocCount + ancapCount + liberalCount + konservativCount + SocialistCount + KommunistCount;

         let PercentageWithIdeologi = (MembersWithIdeologi / serverCount * 100).toFixed(2);

         const chart = {
             type: 'bar',
             data: {
               labels: ['Filosof', 'NatSoc', 'Ancap', 'Liberal', 'Konservativ', 'Socialist', 'Kommunist'],
               datasets: [{
                 label: 'Antal m. Ideologi',
                 data: [FilosofCount, natSocCount, ancapCount, liberalCount, konservativCount, SocialistCount, KommunistCount]
               }]
             }
           }
           const encodedChart = encodeURIComponent(JSON.stringify(chart));

           const chartUrl = `https://quickchart.io/chart?c=${encodedChart}`;

           const chartEmbed = {
             title: 'Ideologier per procent',
             description: 'Serverens ideologier i chart form',
             image: {
               //url: chart.getUrl(),
               url: chartUrl,
             },
           };
           msg.reply({ embed: chartEmbed });
           msg.reply("\nFilosof: " + FilosofPercentage + "%\nNational Socialist: " + NatSocPercentage + "%\nAnarko Kapitalist: " + AncapPercentage + "%\nLiberal: " + LiberalPercentage + "%\nKonservativ: " + KonservativPercentage + "%\nSocialist: " + SocialistPercentage + "%\nKommunist: " + KommunistPercentage + "%\n\n" + PercentageWithIdeologi + "% med Ideologi på serveren");

//         let memberCount = guild.roles.get(roleID).members.size;
//         message.channel.send(memberCount + " members have this role!");
     }
 });


client.on('message', msg => {
    if (msg.content === '!basedbibel')
	{
            fetch('https://redpillapi.com/api/v1/basedbible/?lang=dk&random=true')
             .then(response => response.json())
             .then(data => ReplyRedpill(data));
        function ReplyRedpill(data)
        {
	    msg.reply(data.data);
        }
	}
})

client.on('message', msg => {
    if (msg.content === '!bibel' || msg.content === '!bible')
	{
            fetch('https://labs.bible.org/api/?passage=random&type=json')
             .then(response => response.json())
             .then(data => ReplyRedpill(data));
        function ReplyRedpill(data)
        {
	    msg.reply(data[0].bookname + " " + data[0].chapter + ":" + data[0].verse + "\n" + "\"" + data[0].text + "\"");
        }
	}
})

client.on('message', msg => {
  if (msg.content === '!server')
  msg.channel.send(`Server Name: ${msg.guild.name}\nTotal Members: ${msg.guild.memberCount}`);
})



client.on('message', msg => {
  if (msg.content === '!totalkrieg')
  {
    if (!msg.mentions.users.size)
      return msg.channel.send(redpills);
    const taggedUser = msg.mentions.users.first();

    console.log(taggedUser);
    console.log(taggedUser.username);
    if (taggedUser.username != '')
      msg.channel.send(`|||${taggedUser.username}|||\n`, redpills);
  }
})

client.on('message', msg => {
    if (msg.content === 'der' || msg.content === 'Der')
    msg.reply('Fürher');
})

client.on('message', message => {
     if (message.content.startsWith('!gas')) { 
    let targetMember = message.mentions.members.first();
    if(!targetMember) return message.reply('you need to tag a user in order to gas them!!');
        // message goes below!
        if(message.mentions.members.first().id === "897584389339377694")
        return message.reply("You can't gas the Führer! Go gas yourself!");

        if(message.mentions.members.first().id === "888363525607682089")
        return message.reply("You can't gas the Führer's right hand! Go gas yourself!");

        if(message.mentions.members.first().id === "876891306763386881")
        {
          message.reply("The fuck you trying to do mate?")
          return message.channel.send(`<@${message.author.id}> you just got gassed `, new Discord.MessageAttachment("https://media.tenor.com/images/121b558bd15a577431b843a0bbafaf88/tenor.gif") );

        }

         message.channel.send(`<@${targetMember.user.id}> you just got gassed `, new Discord.MessageAttachment("https://media.tenor.com/images/121b558bd15a577431b843a0bbafaf88/tenor.gif") );
    }
}); 

client.on('message', message => {
  if (message.content.includes("flygtning") || message.content.includes("Flygtning") || message.content === "!flygtninge")
     message.reply(new Discord.MessageAttachment("https://cdn.discordapp.com/attachments/835609255250755594/836685356052905994/ZomboMeme_21042021224819.jpg"));
})

/*
client.on('message', message => {
  if (message.content.includes("Thomas") || message.content.includes("thomas") || message.content === "!thomas")
     message.channel.send("https://www.youtube.com/watch?v=E3ftYnicKYA");
})
*/

client.on('message', message => {
  if (message.content === "!niggers" || message.content === "!nigger")
     message.reply(new Discord.MessageAttachment("https://cdn.discordapp.com/attachments/835609255250755594/836685356052905994/ZomboMeme_21042021224819.jpg"));
})

client.on('message', message => {
  if (message.content.includes("Obama") || message.content.includes("obama") || message.content === "!obama")
     message.reply(new Discord.MessageAttachment("https://cdn.discordapp.com/attachments/835609255250755594/838796913804836904/8mb.video-hLc-0QIa39tg.mp4"));
})

client.on('message', message => {
  if (message.content.includes("Hitler") || message.content.includes("Adolf") || message.content === "!hitler")
     message.reply(new Discord.MessageAttachment("https://cdn.discordapp.com/attachments/835609255250755594/838797233465983047/video0_53-1.mp4"));
})

client.on('message', message => {
  if (message.content === '!faggot')
    message.reply(new Discord.MessageAttachment("https://cdn.discordapp.com/attachments/835609255250755594/835941929216245800/d509372c-1a36-4.mp4"));
})

client.on('message', message => {
  if (message.content === '!troll')
    message.reply(new Discord.MessageAttachment("https://giant.gfycat.com/FrigidMediocreHornedtoad.mp4"));
})

client.on('message', message => {
  if (message.content === '!jøde')
    message.reply(new Discord.MessageAttachment("https://cdn.discordapp.com/attachments/835609255250755594/835958377996746752/DePFTiMWAAAji4j.jpg"));
})

client.on('message', message => {
  if (message.content === '!help')
    message.channel.send ("```Help Page\n !redpill - To throw Redpills \n !faggot - For en god video \n !gas @bruger - For at gasse brugeren \n sieg - heil \n !server - Server info\n !donate - Donate Monero \n !jøde - Hvis nogen opføre sig som en jøde\n And a few more hidden Easter Eggs ;) \n !blackpill - Vis en blackpill \n !bibel - Vis et tilfældigt bibel vers \n !basedbibel - Vis et based bibel vers \n !basedbible - Vis et based bibel vers på engelsk```");
})

client.on('message', msg => {
  if (msg.content === 'sieg' || msg.content === 'Sieg') {
    msg.reply('HEIL!!!');
  }
});

client.on('message', msg => {
  if (msg.content === 'heil' || msg.content === 'Heil') {
    msg.reply('Hitler');
  }
});

client.on('message', msg => {
  if (msg.content === '!donate') {
    msg.channel.send('Monero: "88cPx6Gzv5RWRRJLstUt6hACF1BRKPp1RMka1ukyu2iuHT7iqzkNfMogYq3YdDAC8AAYRqmqQMkCgBXiwdD5Dvqw3LsPGLU"');
  }
});

client.on('message', message => {
  if (message.content.startsWith("!count")) {
    var prefix = "!count"
    const args = message.content.slice(prefix.length).trim().split(' ');
    if (args[0] > 10 && args[0] < 100)
      return message.reply("Det bliver sgu ikke");

    if (args[0] > 100)
    message.reply("ER DU KOMPLET PSYCHOPATH????");
    else {
    for (var i = 1; i <= args[0]; i++)
    {
      message.channel.send(i);
    }
  }
  }
});

client.login(config.token);
